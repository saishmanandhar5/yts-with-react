import logo from './logo.svg';
import './App.css';

import Navbar from './components/Navbar';
import Movies from './components/Movies';
import {
  BrowserRouter,
  Routes,
  Route,

} from "react-router-dom";
import Details from './components/Details';
import Comment from './components/Comment';



function App() {
  return (
    <>
      <div className="main">
          <Navbar />
          <Routes>
            <Route path='/' element={<Movies />} />
            <Route path="/details/:id" element={<Details />} />
            <Route path="/comment/:id" element={<Comment />} />
          </Routes>
      </div>
    </>
  );
}

export default App;
