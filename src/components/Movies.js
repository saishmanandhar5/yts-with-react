import React from 'react'
import '../assets/css/Movies.css'
import axios from 'axios'
import { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom';

export default function Movies() {
    const navigate = useNavigate();
    const [movies, setMovies] = useState([])

    const footerItems= ["api", "dco"]
    useEffect(() => {
        axios.get('https://yts.mx/api/v2/list_movies.json')
            .then((res) => {
                // console.log("hi", res.data.data.movies)
                setMovies(res.data.data.movies);
            })
    }, []);


    const handleClick = (movie) => {
        // console.log('clicked')
        navigate(`/details/${movie.id}`)

    }

    return (
        <>
            <div className="content">
                <div className="text-content flex">



                    <h1 id="txt" >Download YTS YIFY movies:HD smallest size</h1>



                    <p id="welcome">Welcome to the official YTS.MX (.LT) website. Here you can browse and download YIFY movies
                        in excellent 720p, 1080p, 2160p 4K and 3D quality, all at the smallest file size. YTS Movies Torrents.
                    </p>




                </div>
                <div className="movie-content ">


                    {
                        movies.map((movie, idx) => <div className="movie-card" onClick={() => handleClick(movie)} 

                        //todo: meaning of key while using map in react
                        key={idx}
                         >
                            <img src={movie.large_cover_image} alt="movie-background" id="movie" />
                            <p id="movie-title" >{movie.title}</p></div>)
                    }
                </div>
                <div className="footer flex">
                    <ul style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'center' }}>
                        <li>YTS © 2011</li>
                        <li>Blog</li>
                        <li>DMCA</li>
                        <li>API</li>
                        <li>RSS</li>
                        <li>CONTACT</li>
                        <li>BROWSE MOVIES</li>
                        <li>REQUEST</li>
                        <li>LOGIN</li>
                        <li>LANGUAGE</li>
                        <li>EZTV</li>

                    </ul>
                    <p style={{ paddingBottom: '20px' }}>By using this site you agree to and accept our User Agreement, which can be read here.</p>

                </div>
            </div>

        </>
    )
}





