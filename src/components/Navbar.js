import React, { useState } from 'react'
import '../assets/css/Navbar.css'
import logo from './logo.png'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import { faBars } from '@fortawesome/free-solid-svg-icons'
import '../assets/css/common.css'


export default function Navbar() {
  const [open,setOpen]=useState(false);
  
  return (
    <>
    <div className="nav-bar flex">
        <div className="logo flex" style={{ marginLeft:'3%'}}>
            <img src={logo} alt="logo" style={{ height:'60px'}}/>
            <p id='p-nav'>HD movies at the smalles size</p>
        </div>
        <div className="nav-content flex">
            <div className="search flex">
            <FontAwesomeIcon icon={faSearch} id='icon' />
            <input type="text" placeholder='Quick Search' id='quick-search' />
            </div>
            <ul >
                <li>Home</li>
                <li style={{color:'green'}}>4k</li>
                <li>Trending</li>
                <li>Brows Movies</li>
                <li>Register</li>
                <li>Sign In</li>
            </ul>
            <FontAwesomeIcon icon={faBars} id='bar'onClick={()=>setOpen(!open)}/>
        </div>
    </div>

        {
          open && (
            <div className="overalay">

              <div className="content ">
              <ul className='flex-centre'>
                <li>Home</li>
                <li style={{color:'green'}}>4k</li>
                <li>Trending</li>
                <li>Brows Movies</li>
                <li>Register</li>
                <li>Sign In</li>
            </ul>
              </div>
            </div>
          )
        }
    </>
  )
}
