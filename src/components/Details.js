import React from 'react'
import axios from 'axios'
import { useEffect, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import '../assets/css/Details.css'


export default function Details() {
    const navigate= useNavigate();
    const [details,setDetails]=useState([]);
    const {id} = useParams();
    console.log(id)
    useEffect(()=>{

      //use of template literals
        axios.get(`https://yts.mx/api/v2/movie_details.json?movie_id=${id}`)
        .then((res)=>{
            // console.log(res.data.data.movie)
            setDetails(res.data.data.movie);
        })
    },[])
    
  return (
    <>
    <div className="movie-details" >
                  <div className="photo ">
                    {
                        <img src={details.large_cover_image} alt="movie1" id="movie"   />
                    }
                    <div className='detail-buttons flex'>
                       <a href={details?.torrents?.[0].url} download> <button id='downloadbtn' >download</button></a>

                      <button id='commentbtn' onClick={()=>navigate(`/comment/${details.id}`)} >Comments</button>
                    </div>
                  </div>
                  <div className="details">
                       
                      <strong >Title</strong> :{details.title}<br/>
                      <strong>Rating</strong>:{details.rating}<br/>  
                      <strong id='description'>Decription</strong> :{details.description_full}<br/>
                      
                      <strong >Runtime</strong> :{details.runtime}<br/>
                  </div>
                  </div>
                  
    </>
  
)}
